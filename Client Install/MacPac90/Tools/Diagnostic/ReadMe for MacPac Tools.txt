**READ ME**
Re: MacPac Diagnostic tools for troubleshooting workstations
Rev. 09/29/10

The MacPac Diagnostic tool is used to ensure the client workstation has
the most recent version of MacPac files, and to assist us in troubleshooting 
errors. To use the tool:

1. Run the mpDiagnose.exe executable file by double-clicking on the file
from Explorer. This file is located in your ...\MacPac\MacPac90\Tools\Diagnostic
folder. The exact location depends on where your MacPac folders are located.

This tool will launch Word and produce a Word document with a list of all 
MacPac files on that workstation, including folder names, files names, file types, 
modified dates, file sizes. Please read the footnotes to determine which 
file dates may vary from the master MacPac set. This file will also include 
a list of the system files for that workstation, including file name, version, 
date, and size.  The contents of MacPac.ini, User.ini, MPN90.ini, NumTOC.ini, CI.ini
and CIUser.ini will be listed at the end of the document.

2. Save the Word document generated and send it to your MacPac project manager. 
We use the information in the file to compare files with a master MacPac workstation.



ARGUMENTS

-nb (no boilerplate files)